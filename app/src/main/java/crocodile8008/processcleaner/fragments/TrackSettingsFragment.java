package crocodile8008.processcleaner.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import crocodile8008.processcleaner.App;
import crocodile8008.processcleaner.R;
import crocodile8008.processcleaner.helpers.SeekBarChangeListener;
import crocodile8008.processcleaner.utils.Preferences;

import static android.content.SharedPreferences.*;

/**
 * Created in 2016
 */
public class TrackSettingsFragment extends Fragment {

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("HH:mm:ss", Locale.US);

    @NonNull private final Preferences preferences = App.component().getPreferences();

    @Bind(R.id.switch1) Switch enabledSwitch;
    @Bind(R.id.textView3) TextView textViewLastCheck;

    @Bind(R.id.seekBar) SeekBar thresholdSeekBar;
    @Bind(R.id.textView) TextView textViewThreshold;

    @Bind(R.id.seekBar2) SeekBar intervalSeekBar;
    @Bind(R.id.textView2) TextView textViewInterval;

    @NonNull
    private final OnSharedPreferenceChangeListener preferenceListener = new OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (Preferences.KEY_LAST_AUTO_CHECK.equals(key)) {
                updateAutoCheckTextView();
            }
        }
    };

    //*********************************************************************************************

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tracking_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setupEnabledSwitch();

        setupThresholdSeekBar();
        updateThresholdText();

        setupIntervalSeekBar();
        updateIntervalText();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateAutoCheckTextView();
        preferences.getPreferences().registerOnSharedPreferenceChangeListener(preferenceListener);
    }

    @Override
    public void onPause() {
        preferences.getPreferences().unregisterOnSharedPreferenceChangeListener(preferenceListener);
        super.onPause();
    }

    //*********************************************************************************************

    private void setupEnabledSwitch() {
        enabledSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preferences.setMemoryTrackingEnabled(isChecked);
                updateEnables();
            }
        });
        enabledSwitch.setChecked(preferences.isMemoryTrackingEnabled());
        updateEnables();
    }

    private void updateEnables() {
        thresholdSeekBar.setEnabled(preferences.isMemoryTrackingEnabled());
        intervalSeekBar.setEnabled(preferences.isMemoryTrackingEnabled());
    }

    //*********************************************************************************************

    private void setupThresholdSeekBar() {
        thresholdSeekBar.setOnSeekBarChangeListener(new SeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                checkSeekBarForMinValue(seekBar, 1);
                preferences.setAutoCleanThreshold(seekBar.getProgress());
                updateThresholdText();
            }
        });
        thresholdSeekBar.setProgress(preferences.getAutoCleanThreshold());
    }

    private void updateThresholdText() {
        textViewThreshold.setText(
                String.format(getString(R.string.auto_clean_threshold), preferences.getAutoCleanThreshold()));
    }

    //*********************************************************************************************

    private void setupIntervalSeekBar() {
        intervalSeekBar.setOnSeekBarChangeListener(new SeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                checkSeekBarForMinValue(seekBar, millisToMin(Preferences.MIN_INTERVAL));
                preferences.setAutoCleanInterval(minToMillis(seekBar.getProgress()));
                updateIntervalText();
            }
        });
        intervalSeekBar.setProgress(millisToMin(preferences.getAutoCleanInterval()));
    }

    private void checkSeekBarForMinValue(SeekBar seekBar, int min) {
        if (seekBar.getProgress() < min) {
            seekBar.setProgress(min);
        }
    }

    private void updateIntervalText() {
        textViewInterval.setText(
                String.format(
                        getString(R.string.auto_clean_interval),
                        millisToMin(preferences.getAutoCleanInterval())
                ));
    }

    private long minToMillis(int minutes) {
        return minutes * 60 * 1000;
    }

    private int millisToMin(long millis) {
        return (int) (millis / 1000 / 60);
    }

    //*********************************************************************************************

    private void updateAutoCheckTextView() {
        long last = preferences.getLastAutoCheck();
        if (last != 0) {
            String lastCheck = SIMPLE_DATE_FORMAT.format(new Date(last));
            textViewLastCheck.setText(String.format(getString(R.string.last), lastCheck));
        }
    }

}
