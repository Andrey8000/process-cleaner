package crocodile8008.processcleaner.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import crocodile8008.processcleaner.App;
import crocodile8008.processcleaner.R;
import crocodile8008.processcleaner.utils.MemoryHelper;
import crocodile8008.processcleaner.utils.Preferences;
import crocodile8008.processcleaner.utils.ProcessKiller;

/**
 * Created in 2016
 */
public class MemoryControlFragment extends Fragment {

    @NonNull private final MemoryHelper memoryHelper = App.component().getMemoryHelper();
    @NonNull private final ProcessKiller processKiller = App.component().getProcessKiller();

    @Bind(R.id.textView1) TextView textViewTotal;
    @Bind(R.id.textView2) TextView textViewUsed;
    @Bind(R.id.textView3) TextView textViewPercent;
    @Bind(R.id.button1) Button buttonKillProcesses;
    @Bind(R.id.progressBar1) ProgressBar progressBar;

    private final Handler handler = new Handler();
    private final long AUTO_UPD_PERIOD_MILLS = 1000;
    private final Runnable periodicalUpdateRunnable = new Runnable() {
        public void run() {
            updateUI();
            handler.postDelayed(periodicalUpdateRunnable, AUTO_UPD_PERIOD_MILLS);
        }
    };

    //*********************************************************************************************

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mem_control, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        updateUI();
        buttonKillProcesses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                killProcessesAsync();
            }
        });
    }

    //*********************************************************************************************

    private void killProcessesAsync() {
        processKiller.killProcessesAsync(new ProcessKiller.Listener() {
            @Override
            public void onFinish() {
                updateUI();
            }
        });
    }

    private void updateUI() {
        memoryHelper.updateMemoryUsage();
        int percents = memoryHelper.getUsedInPercents();

        textViewTotal.setText(String.format(getString(R.string.total_memory), memoryHelper.getTotalMemoryMb()));
        textViewUsed.setText(String.format(getString(R.string.used_memory), memoryHelper.getUsedMemoryMb()));
        textViewPercent.setText(percents + "%");
        updatePercentTextViewColor(percents);
        progressBar.setProgress(percents);
    }

    private void updatePercentTextViewColor(int percent) {
        if (percent < Preferences.WARNING_PERCENT) {
            textViewPercent.setTextColor(getResources().getColor(R.color.accent));
        } else if (percent < Preferences.DANGER_PERCENT) {
            textViewPercent.setTextColor(getResources().getColor(R.color.orange));
        } else {
            textViewPercent.setTextColor(getResources().getColor(R.color.red));
        }
    }

    //*********************************************************************************************

    @Override
    public void onResume() {
        super.onResume();
        startPeriodicalInfoUpdate();
    }

    @Override
    public void onPause() {
        stopPeriodicalInfoUpdate();
        super.onPause();
    }

    private void startPeriodicalInfoUpdate() {
        handler.removeCallbacks(periodicalUpdateRunnable);
        handler.postDelayed(periodicalUpdateRunnable, AUTO_UPD_PERIOD_MILLS);
    }

    private void stopPeriodicalInfoUpdate() {
        handler.removeCallbacks(periodicalUpdateRunnable);
    }
}
