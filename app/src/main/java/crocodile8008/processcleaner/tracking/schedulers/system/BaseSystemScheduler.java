package crocodile8008.processcleaner.tracking.schedulers.system;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;

import crocodile8008.processcleaner.helpers.EmptyRunnable;
import crocodile8008.processcleaner.helpers.Lo;
import crocodile8008.processcleaner.tracking.schedulers.Scheduler;

/**
 * Created by Andrei Riik in 2016
 */
public abstract class BaseSystemScheduler implements Scheduler {

    protected static final long DELAY_TO_STOP_SERVICE = DateUtils.SECOND_IN_MILLIS * 5;

    @NonNull protected final Context context;
    @NonNull protected final Handler handler = new Handler();
    @NonNull protected Runnable externalRunnable = EmptyRunnable.INSTANCE;

    @NonNull protected final Runnable stopServiceRunnable = new Runnable() {
        @Override
        public void run() {
            stopService();
        }
    };

    protected long lastTimestamp;

    //*********************************************************************************************

    protected BaseSystemScheduler(@NonNull Context context) {
        this.context = context;
        Lo.v(getClass().getSimpleName() + " created");
    }

    public long getLastTimestamp() {
        return lastTimestamp;
    }

    @Override
    public final void start(long intervalFromNow, @NonNull Runnable externalRunnable) {
        this.externalRunnable = externalRunnable;
        handler.removeCallbacks(stopServiceRunnable);
        lastTimestamp = System.currentTimeMillis();

        onStart(intervalFromNow, externalRunnable);
    }

    @Override
    public final void stop() {
        onStop();
        handler.postDelayed(stopServiceRunnable, DELAY_TO_STOP_SERVICE);
    }

    @Override
    public final void onTriggered() {
        Lo.i(getClass().getSimpleName() + " onTriggered");
        externalRunnable.run();
    }

    protected abstract void onStart(long intervalFromNow, @NonNull Runnable externalRunnable);

    protected abstract void onStop();

    protected abstract void stopService();
}
