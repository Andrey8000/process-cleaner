package crocodile8008.processcleaner.tracking.schedulers.app;

import android.os.Handler;
import android.support.annotation.NonNull;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;

import crocodile8008.processcleaner.helpers.Lo;
import crocodile8008.processcleaner.helpers.EmptyRunnable;
import crocodile8008.processcleaner.tracking.schedulers.Scheduler;

/**
 * Created in 2016
 *
 * On some devices alarm may be postponed by the system,
 * so make fallback with Handler.post while app is running
 */
@Singleton
public class ApplicationScheduler implements Scheduler {

    @NonNull private final Handler handler = new Handler();
    @NonNull private Runnable externalRunnable = EmptyRunnable.INSTANCE;

    @NonNull private final Runnable workRunnable = new Runnable() {
        @Override
        public void run() {
            onTriggered();
        }
    };

    @Inject
    public ApplicationScheduler() {}

    @Override
    public void start(long interval, @NonNull Runnable externalRunnable) {
        this.externalRunnable = externalRunnable;
        handler.postDelayed(workRunnable, interval);
        Lo.d("ApplicationScheduler start interval: " + interval
                + "   " + new Date(System.currentTimeMillis() + interval));
    }

    @Override
    public void stop() {
        handler.removeCallbacks(workRunnable);
        Lo.v("ApplicationScheduler stop");
    }

    @Override
    public void onTriggered() {
        Lo.i("ApplicationScheduler onTriggered");
        externalRunnable.run();
    }
}
