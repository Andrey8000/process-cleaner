package crocodile8008.processcleaner.tracking.schedulers.system.job;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;

import crocodile8008.processcleaner.helpers.Lo;
import crocodile8008.processcleaner.tracking.schedulers.system.BaseSystemScheduler;

/**
 * Created in 2016
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
@Singleton
public class JobSystemScheduler extends BaseSystemScheduler {

    private static final int CONSTANT_JOB_ID = 123;

    @Inject
    public JobSystemScheduler(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onStart(long intervalFromNow, @NonNull Runnable externalRunnable) {
        Lo.d("JobScheduler start interval: " + intervalFromNow + "   "
                + new Date(System.currentTimeMillis() + intervalFromNow));

        ComponentName componentName = new ComponentName(context, JobTrackingService.class);
        JobInfo jobInfo = new JobInfo.Builder(CONSTANT_JOB_ID, componentName)
                .setPeriodic(intervalFromNow)
                .build();
        getJobScheduler().schedule(jobInfo);
        Lo.v("JobScheduler schedule: " + jobInfo);
    }

    @Override
    protected void onStop() {
        getJobScheduler().cancel(CONSTANT_JOB_ID);
        Lo.v("JobScheduler stop");
    }

    @Override
    protected void stopService() {
        context.stopService(JobTrackingService.getIntentForService(context));
    }

    @NonNull
    private android.app.job.JobScheduler getJobScheduler() {
        return (android.app.job.JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
    }

}
