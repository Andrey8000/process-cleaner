package crocodile8008.processcleaner.tracking.schedulers.system.alarm;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import crocodile8008.processcleaner.App;
import crocodile8008.processcleaner.helpers.Lo;
import crocodile8008.processcleaner.tracking.schedulers.system.BaseSystemScheduler;

/**
 * Created by Andrei Riik in 2016
 */
public class AlarmTrackingService extends Service {

    public static final String TIMESTAMP = "timestamp";
    @NonNull private final BaseSystemScheduler scheduler = App.component().getBaseSystemScheduler();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //*********************************************************************************************

    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        long timestamp = getTimestamp(intent);
        Lo.v("AlarmTrackingService onStartCommand " + timestamp + "  " + this + "  " + intent);

        if (intent == null || timestamp == scheduler.getLastTimestamp()) {
            scheduler.onTriggered();
        } else {
            Lo.v("AlarmTrackingService onStartCommand with wrong timestamp");
        }
        return START_STICKY;
    }

    private long getTimestamp(@Nullable Intent intent) {
        long fallback = -1;
        if (intent == null) {
            return fallback;
        }
        return intent.getLongExtra("timestamp", fallback);
    }

    @Override
    public void onDestroy() {
        Lo.v("AlarmTrackingService onDestroy " + this);
        super.onDestroy();
    }

    //*********************************************************************************************

    public static Intent getIntentForService(@NonNull Context context, long timestamp) {
        final Intent intent = new Intent(context, AlarmTrackingService.class);
        intent.putExtra(TIMESTAMP, timestamp);
        return intent;
    }

    public static PendingIntent getPendingIntent(@NonNull Context context, long timestamp) {
        return PendingIntent.getService(
                context, 0, getIntentForService(context, timestamp), PendingIntent.FLAG_UPDATE_CURRENT);
    }

}
