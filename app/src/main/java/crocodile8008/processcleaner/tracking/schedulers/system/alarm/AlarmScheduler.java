package crocodile8008.processcleaner.tracking.schedulers.system.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;

import crocodile8008.processcleaner.helpers.Lo;
import crocodile8008.processcleaner.tracking.schedulers.system.BaseSystemScheduler;

/**
 * Created in 2016
 */
@Singleton
public class AlarmScheduler extends BaseSystemScheduler {

    @Inject
    public AlarmScheduler(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onStart(long intervalFromNow, @NonNull Runnable externalRunnable) {
        PendingIntent pendingIntent = AlarmTrackingService.getPendingIntent(context, lastTimestamp);
        long triggeredTime = System.currentTimeMillis() + intervalFromNow;
        Lo.d("AlarmScheduler start interval: " + intervalFromNow + "   " + new Date(triggeredTime));

        if (Build.VERSION.SDK_INT < 19) {
            getAlarmManager().set(AlarmManager.RTC_WAKEUP, triggeredTime, pendingIntent);
        } else {
            getAlarmManager().setExact(AlarmManager.RTC_WAKEUP, triggeredTime, pendingIntent);
        }
    }

    @Override
    protected void onStop() {
        PendingIntent pendingIntent = AlarmTrackingService.getPendingIntent(context, 0);
        getAlarmManager().cancel(pendingIntent);
        Lo.v("AlarmScheduler stop");
    }

    @Override
    protected void stopService() {
        context.stopService(AlarmTrackingService.getIntentForService(context, 0));
    }

    @NonNull
    private AlarmManager getAlarmManager() {
        return (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }
}
