package crocodile8008.processcleaner.tracking.schedulers;

import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

import crocodile8008.processcleaner.helpers.EmptyRunnable;
import crocodile8008.processcleaner.tracking.schedulers.app.ApplicationScheduler;
import crocodile8008.processcleaner.tracking.schedulers.system.BaseSystemScheduler;

/**
 * Created by Andrei Riik in 2016
 */
@Singleton
public class CompositeScheduler implements Scheduler {

    @NonNull private final Scheduler systemScheduler;
    @NonNull private final Scheduler applicationScheduler;
    @NonNull private Runnable externalRunnable = EmptyRunnable.INSTANCE;

    //*********************************************************************************************

    @Inject
    public CompositeScheduler(@NonNull BaseSystemScheduler systemScheduler,
                              @NonNull ApplicationScheduler applicationScheduler) {
        this.systemScheduler = systemScheduler;
        this.applicationScheduler = applicationScheduler;
    }

    @Override
    public void start(long intervalFromNow, @NonNull Runnable externalRunnable) {
        this.externalRunnable = externalRunnable;
        stop();
        Runnable internalRunnable = new CompositeTriggerRunnable();
        systemScheduler.start(intervalFromNow, internalRunnable);
        applicationScheduler.start(intervalFromNow, internalRunnable);
    }

    @Override
    public void stop() {
        systemScheduler.stop();
        applicationScheduler.stop();
    }

    @Override
    public void onTriggered() {
        stop();
        externalRunnable.run();
    }

    //*********************************************************************************************

    private class CompositeTriggerRunnable implements Runnable {

        private boolean finished;
        @Override
        public void run() {
            synchronized (CompositeTriggerRunnable.this) {
                if (finished) {
                    return;
                }
                finished = true;
                onTriggered();
            }
        }
    }
}
