package crocodile8008.processcleaner.tracking.schedulers.system.job;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import crocodile8008.processcleaner.App;
import crocodile8008.processcleaner.helpers.Lo;
import crocodile8008.processcleaner.tracking.schedulers.system.BaseSystemScheduler;

/**
 * Created by Andrei Riik in 2016
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class JobTrackingService extends JobService {

    @NonNull private final BaseSystemScheduler scheduler = App.component().getBaseSystemScheduler();

    @Override
    public boolean onStartJob(JobParameters params) {
        Lo.v("JobTrackingService onStartJob ");
        scheduler.onTriggered();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Lo.v("JobTrackingService onStopJob ");
        return false;
    }

    //*********************************************************************************************

    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Lo.v("JobTrackingService onStartCommand " + this + "  " + intent);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Lo.v("JobTrackingService onDestroy " + this);
        super.onDestroy();
    }

    //*********************************************************************************************

    public static Intent getIntentForService(@NonNull Context context) {
        return new Intent(context, JobTrackingService.class);
    }
}
