package crocodile8008.processcleaner.tracking.schedulers;

import android.support.annotation.NonNull;

/**
 * Created by Andrei Riik in 2016
 */
public interface Scheduler {

    void start(long intervalFromNow, @NonNull Runnable externalRunnable);

    void stop();

    void onTriggered();
}
