package crocodile8008.processcleaner.tracking;

import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

import crocodile8008.processcleaner.utils.MemoryHelper;
import crocodile8008.processcleaner.utils.Preferences;
import crocodile8008.processcleaner.utils.ProcessKiller;

/**
 * Created by Andrei Riik in 2016
 */
@Singleton
public class MemoryChecker implements Runnable {

    @NonNull private final MemoryHelper memoryHelper;
    @NonNull private final ProcessKiller processKiller;
    @NonNull private final Preferences preferences;

    @Inject
    public MemoryChecker(@NonNull MemoryHelper memoryHelper, @NonNull ProcessKiller processKiller,
                         @NonNull Preferences preferences) {
        this.memoryHelper = memoryHelper;
        this.processKiller = processKiller;
        this.preferences = preferences;
    }

    @Override
    public void run() {
        preferences.setLastAutoCheck(System.currentTimeMillis());
        if (memoryHelper.getUsedInPercents() >= preferences.getAutoCleanThreshold()) {
            processKiller.killProcessesAsync(null);
        }
    }
}
