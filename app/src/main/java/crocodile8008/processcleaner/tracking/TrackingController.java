package crocodile8008.processcleaner.tracking;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

import crocodile8008.processcleaner.helpers.Lo;
import crocodile8008.processcleaner.tracking.schedulers.CompositeScheduler;
import crocodile8008.processcleaner.tracking.schedulers.Scheduler;
import crocodile8008.processcleaner.utils.Preferences;

import static android.content.SharedPreferences.*;
import static crocodile8008.processcleaner.utils.Preferences.*;

/**
 * Created in 2016
 */
@Singleton
public class TrackingController {

    @NonNull private final Preferences preferences;
    @NonNull private final MemoryChecker memoryChecker;
    @NonNull private final Scheduler scheduler;
    @NonNull private final Runnable triggerRunnable = new TriggerRunnable();
    @SuppressWarnings("FieldCanBeLocal")
    @NonNull private final OnSharedPreferenceChangeListener prefListener = new PreferenceChangeListener();

    //*********************************************************************************************

    @Inject
    public TrackingController(@NonNull Preferences preferences,
                              @NonNull CompositeScheduler scheduler,
                              @NonNull MemoryChecker memoryChecker) {
        this.preferences = preferences;
        this.scheduler = scheduler;
        this.memoryChecker = memoryChecker;
        preferences.getPreferences().registerOnSharedPreferenceChangeListener(prefListener);
    }

    public void updateRunningState() {
        boolean enabled = preferences.isMemoryTrackingEnabled();
        Lo.v("TrackingServiceController updateRunningState: " + enabled);
        if (enabled) {
            runAndSchedule();
        } else {
            scheduler.stop();
        }
    }

    private void runAndSchedule() {
        memoryChecker.run();
        scheduler.start(preferences.getAutoCleanInterval(), triggerRunnable);
    }

    //*********************************************************************************************

    private class TriggerRunnable implements Runnable {
        @Override
        public void run() {
            runAndSchedule();
        }
    }

    private class PreferenceChangeListener implements OnSharedPreferenceChangeListener {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (KEY_MEM_TRACKING_ENABLED.equals(key) || KEY_AUTO_CLEAN_INTERVAL.equals(key)) {
                updateRunningState();
            }
        }
    }

}
