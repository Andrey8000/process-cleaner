package crocodile8008.processcleaner.activity.main;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;

import crocodile8008.processcleaner.R;

/**
 * Created by Andrei Riik in 2016
 */
public class ToolbarSlideController {

    private int lastScroll;

    public ToolbarSlideController(@NonNull AppCompatActivity activity, @NonNull final Toolbar toolbar) {
        final ScrollView scrollView = (ScrollView) activity.findViewById(R.id.scrollView);
        if (scrollView == null) {
            return;
        }
        setupScrollListener(toolbar, scrollView);
        setupTouchListener(toolbar, scrollView);
    }

    private void setupScrollListener(@NonNull final Toolbar toolbar, @NonNull final ScrollView scrollView) {
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY();
                int diff = lastScroll - scrollY;
                float translationY = toolbar.getTranslationY();

                // to bottom
                if (diff > 0 && translationY < 0) {
                    float futureY = translationY + diff;
                    if (futureY > 0) futureY = 0;
                    toolbar.setTranslationY(futureY);

                // to top
                } else if (diff < 0 && translationY > -toolbar.getHeight()) {
                    float futureY = translationY + diff;
                    if (futureY < -toolbar.getHeight()) futureY = -toolbar.getHeight();
                    toolbar.setTranslationY(futureY);
                }

                lastScroll = scrollY;
            }
        });
    }

    private void setupTouchListener(@NonNull final Toolbar toolbar, @NonNull ScrollView scrollView) {
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_CANCEL == event.getAction() || MotionEvent.ACTION_UP == event.getAction()) {
                    animateToNearest(toolbar);
                }
                return false;
            }
        });
    }

    private void animateToNearest(@NonNull final Toolbar toolbar) {
        float translationY = toolbar.getTranslationY();
        int topY = -toolbar.getHeight();
        if (translationY == 0 || translationY == topY) {
            return;
        }

        int nearestY = translationY > topY / 2 ? 0 : topY;
        if (lastScroll < toolbar.getHeight() / 2) {
            nearestY = 0;
        }

        toolbar.animate().translationY(nearestY).setDuration(200).start();
    }
}
