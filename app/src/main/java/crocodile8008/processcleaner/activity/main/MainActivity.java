package crocodile8008.processcleaner.activity.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import crocodile8008.processcleaner.R;
import crocodile8008.processcleaner.fragments.MemoryControlFragment;
import crocodile8008.processcleaner.fragments.TrackSettingsFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        new ToolbarSlideController(this, toolbar);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_1, new MemoryControlFragment())
                .replace(R.id.frame_2, new TrackSettingsFragment())
                .commit();

    }

}
