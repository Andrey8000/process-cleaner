package crocodile8008.processcleaner;

import android.app.Application;

/**
 * Created in 2016
 */
public class App extends Application {

    private static App instance;

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        buildComponents();
        initMemTrackingController();
    }

    private void buildComponents() {
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    private void initMemTrackingController() {
        appComponent.getTrackingServiceController().updateRunningState();
    }

    public static App getInstance() {
        return instance;
    }

    public static AppComponent component() {
        return instance.appComponent;
    }

}
