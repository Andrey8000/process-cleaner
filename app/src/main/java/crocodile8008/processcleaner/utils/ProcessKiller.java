package crocodile8008.processcleaner.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;

import crocodile8008.processcleaner.helpers.Lo;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Andrey Riyk
 */
@Singleton
public class ProcessKiller {

    @NonNull private final Context context;
    @NonNull private final Scheduler scheduler = Schedulers.from(Executors.newSingleThreadExecutor());
    @Nullable private final String selfPackage;

    @Inject
    public ProcessKiller(@NonNull Context context) {
        this.context = context;
        selfPackage = context.getPackageName();
    }

    public void killProcessesAsync(@Nullable final Listener listener) {
        Observable
                .create(new Observable.OnSubscribe<Object>() {
                    @Override
                    public void call(Subscriber<? super Object> subscriber) {
                        killProcesses();
                        subscriber.onNext(null);
                        subscriber.onCompleted();
                    }
                })
                .subscribeOn(scheduler)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object object) {
                        if (listener != null) {
                            listener.onFinish();
                        }
                    }
                });
    }

    public void killProcesses() {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        final List<ActivityManager.RunningAppProcessInfo> processes = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo pid : processes) {
            if (!isSelfProcess(pid)) {
                am.killBackgroundProcesses(pid.processName);
            }
        }

        final List<ActivityManager.RunningServiceInfo> services = am.getRunningServices(1024);
        for (ActivityManager.RunningServiceInfo pid : services) {
            if (!isSelfService(pid)) {
                am.killBackgroundProcesses(pid.process);
            }
        }

        Lo.i("ProcessKiller kill processes: " + processes.size() + "   services: " + services.size());
    }

    private boolean isSelfProcess(@Nullable ActivityManager.RunningAppProcessInfo pid) {
        if (pid == null || pid.processName == null) {
            return false;
        }
        return pid.processName.equals(selfPackage);
    }

    private boolean isSelfService(@Nullable ActivityManager.RunningServiceInfo pid) {
        if (pid == null || pid.service == null || pid.service.getPackageName() == null) {
            return false;
        }
        return pid.service.getPackageName().equals(selfPackage);
    }

    public interface Listener {
        void onFinish();
    }
}
