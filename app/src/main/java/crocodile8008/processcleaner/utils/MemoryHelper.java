package crocodile8008.processcleaner.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Andrey Riyk
 */
@Singleton
public class MemoryHelper {

    @NonNull private final ActivityManager.MemoryInfo memoryInfo;
    @NonNull private final ActivityManager activityManager;

    private long totalMemoryMb;
    private long usedMemoryMb;

    @Inject
    public MemoryHelper(@NonNull Context context) {
        memoryInfo = new ActivityManager.MemoryInfo();
        activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        updateMemoryUsage();
    }

    public void updateMemoryUsage() {
        activityManager.getMemoryInfo(memoryInfo);
        totalMemoryMb = memoryInfo.totalMem / 1048576L;
        usedMemoryMb = (memoryInfo.totalMem - memoryInfo.availMem) / 1048576L;
    }

    public long getTotalMemoryMb() {
        return totalMemoryMb;
    }

    public long getUsedMemoryMb() {
        return usedMemoryMb;
    }

    public int getUsedInPercents() {
        return (int) (((float) getUsedMemoryMb() / (float) getTotalMemoryMb()) * 100);
    }
}
