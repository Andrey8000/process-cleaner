package crocodile8008.processcleaner.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created in 2016
 */
@Singleton
public class Preferences {

    public static final String APP_PREFS = "app_prefs";

    public static final String KEY_MEM_TRACKING_ENABLED = "mem_tracking_enabled";
    public static final String KEY_AUTO_CLEAN_THRESHOLD = "auto_clean_threshold";
    public static final String KEY_AUTO_CLEAN_INTERVAL = "auto_clean_interval";
    public static final String KEY_LAST_AUTO_CHECK = "last_auto_check";

    public static final long DEFAULT_INTERVAL = 30 * DateUtils.MINUTE_IN_MILLIS;
    public static final long MIN_INTERVAL = DateUtils.MINUTE_IN_MILLIS;

    public static final int WARNING_PERCENT = 70;
    public static final int DANGER_PERCENT = 80;

    @NonNull private final SharedPreferences preferences;

    @Inject
    public Preferences(@NonNull Context context) {
        preferences = context.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
    }

    @NonNull
    public SharedPreferences getPreferences() {
        return preferences;
    }

    //*********************************************************************************************

    public void setMemoryTrackingEnabled(boolean enabled) {
        preferences.edit().putBoolean(KEY_MEM_TRACKING_ENABLED, enabled).apply();
    }

    public boolean isMemoryTrackingEnabled() {
        return preferences.getBoolean(KEY_MEM_TRACKING_ENABLED, false);
    }

    //*********************************************************************************************

    public void setAutoCleanThreshold(int threshold) {
        preferences.edit().putInt(KEY_AUTO_CLEAN_THRESHOLD, threshold).apply();
    }

    public int getAutoCleanThreshold() {
        return preferences.getInt(KEY_AUTO_CLEAN_THRESHOLD, WARNING_PERCENT);
    }

    //*********************************************************************************************

    public void setAutoCleanInterval(long millis) {
        preferences.edit().putLong(KEY_AUTO_CLEAN_INTERVAL, millis).apply();
    }

    public long getAutoCleanInterval() {
        return preferences.getLong(KEY_AUTO_CLEAN_INTERVAL, DEFAULT_INTERVAL);
    }

    //*********************************************************************************************

    public void setLastAutoCheck(long millis) {
        preferences.edit().putLong(KEY_LAST_AUTO_CHECK, millis).apply();
    }

    public long getLastAutoCheck() {
        return preferences.getLong(KEY_LAST_AUTO_CHECK, 0);
    }
}
