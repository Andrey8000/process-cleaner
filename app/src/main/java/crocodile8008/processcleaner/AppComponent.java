package crocodile8008.processcleaner;

import javax.inject.Singleton;

import crocodile8008.processcleaner.tracking.TrackingController;
import crocodile8008.processcleaner.tracking.schedulers.system.BaseSystemScheduler;
import crocodile8008.processcleaner.utils.MemoryHelper;
import crocodile8008.processcleaner.utils.Preferences;
import crocodile8008.processcleaner.utils.ProcessKiller;
import dagger.Component;

/**
 * Created in 2016
 */
@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    MemoryHelper getMemoryHelper();
    ProcessKiller getProcessKiller();
    Preferences getPreferences();
    TrackingController getTrackingServiceController();
    BaseSystemScheduler getBaseSystemScheduler();
}
