package crocodile8008.processcleaner.helpers;

/**
 * Created by Andrei Riik in 2016
 */
public class EmptyRunnable implements Runnable {

    public static final EmptyRunnable INSTANCE = new EmptyRunnable();

    private EmptyRunnable() {}

    @Override
    public void run() {

    }
}
