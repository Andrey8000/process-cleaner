package crocodile8008.processcleaner;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import crocodile8008.processcleaner.tracking.schedulers.system.BaseSystemScheduler;
import crocodile8008.processcleaner.tracking.schedulers.system.alarm.AlarmScheduler;
import crocodile8008.processcleaner.tracking.schedulers.system.job.JobSystemScheduler;
import dagger.Module;
import dagger.Provides;

/**
 * Created in 2016
 */
@Module
@Singleton
public class AppModule {

    @NonNull
    private final Context context;

    public AppModule(@NonNull Application application) {
        this.context = application;
    }

    @Singleton
    @Provides
    Context providesContext() {
        return context;
    }

    @Singleton
    @Provides
    BaseSystemScheduler providesBaseSystemScheduler() {
        return Build.VERSION.SDK_INT >= 21 ? new JobSystemScheduler(context) : new AlarmScheduler(context);
    }
}
