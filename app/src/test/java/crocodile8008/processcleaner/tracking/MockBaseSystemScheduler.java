package crocodile8008.processcleaner.tracking;

import android.content.Context;
import android.support.annotation.NonNull;

import crocodile8008.processcleaner.tracking.schedulers.system.BaseSystemScheduler;

/**
 * Created by Andrei Riik in 2016
 */
public class MockBaseSystemScheduler extends BaseSystemScheduler {

    protected MockBaseSystemScheduler(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onStart(long intervalFromNow, @NonNull Runnable externalRunnable) {

    }

    @Override
    protected void onStop() {

    }

    @Override
    protected void stopService() {

    }
}
