package crocodile8008.processcleaner.tracking;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import crocodile8008.processcleaner.tracking.schedulers.CompositeScheduler;
import crocodile8008.processcleaner.tracking.schedulers.app.ApplicationScheduler;
import crocodile8008.processcleaner.utils.Preferences;

import static org.mockito.Mockito.*;

@Config(manifest=Config.NONE)
@RunWith(RobolectricTestRunner.class)
public class TrackingControllerTest {

    private TrackingController trackingController;
    private Preferences preferences;
    private CompositeScheduler compositeScheduler;
    private MemoryChecker checker;

    //*********************************************************************************************

    @Before
    public void setUp() {
        Activity activity = Robolectric.buildActivity(Activity.class).get();
        preferences = mockPreferences();
        compositeScheduler = new CompositeScheduler(new MockBaseSystemScheduler(activity), new ApplicationScheduler());
        checker = mock(MemoryChecker.class);
        trackingController = new TrackingController(preferences, compositeScheduler, checker);
    }

    @NonNull
    private Preferences mockPreferences() {
        Preferences preferences = mock(Preferences.class);
        when(preferences.getAutoCleanInterval()).thenReturn(1_000L);
        when(preferences.getPreferences()).thenReturn(mock(SharedPreferences.class));
        return preferences;
    }

    //*********************************************************************************************

    @Test
    public void checkerRunWithUpdatingAndTriggering() {
        setEnabledInPrefs(true);
        trackingController.updateRunningState();
        compositeScheduler.onTriggered();
        compositeScheduler.onTriggered();
        trackingController.updateRunningState();
        verify(checker, times(4)).run();
    }

    @Test
    public void checkerNotRunIfDisabledInPrefs() {
        setEnabledInPrefs(false);
        trackingController.updateRunningState();
        compositeScheduler.onTriggered();
        verify(checker, never()).run();
    }

    private void setEnabledInPrefs(boolean value) {
        when(preferences.isMemoryTrackingEnabled()).thenReturn(value);
    }

}